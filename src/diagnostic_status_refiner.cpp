/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sezaljain90@gmail.com>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name : diagnostic_status_refiner.cpp
* Purpose :
* Creation Date : 20-05-2015
* Last Modified : Wed 20 May 2015 11:23:42 AM EDT
* Created By :   Sezal Jain (sezaljain90@gmail.com)
_._._._._._._._._._._._._._._._._._._._._.*/

#include <diagnostic_status_refiner/diagnostic_status_refiner.h>
namespace ca{

bool DiagStatusRefiner::AggregateEntries(diagnostic_updater::DiagnosticStatusWrapper& aggregated){
  CompressEntries();
  bool notset=true;
  std::vector<bool> indices;
  for(RefinerEntries::iterator itr(entries_.begin()), end_itr(entries_.end());
       itr != end_itr;
       ++itr) {
       if(itr->stat_.level==0||itr->stat_.level==3) indices.push_back(false);
       else{
         if(ros::Time::now()-itr->t_>itr->expiration_ && itr->expiration_!=ros::Duration(-1.0)){
           indices.push_back(false);
         }
         else{
           if(itr->stat_.level==1){
             if(aggregated.level==2) indices.push_back(false);
             else {
               aggregated.summary(itr->stat_.level,itr->stat_.message);
               notset=false;
               aggregated.add(itr->id_,boost::to_string(itr->t_)+" : "+aggregated.message);
               if(itr->expiration_==ros::Duration(-1.0)) indices.push_back(false);
               else indices.push_back(true);
             }
           }
           else if (itr->stat_.level==2){
               aggregated.summary(itr->stat_.level,itr->stat_.message);
               notset=false;
               aggregated.add(itr->id_,boost::to_string(itr->t_)+" : "+aggregated.message);
               if(itr->expiration_==ros::Duration(-1.0)) indices.push_back(false);
               else indices.push_back(true);           
           }
           else indices.push_back(false);
         }
       }
  }
  if(notset) aggregated.summary(diagnostic_msgs::DiagnosticStatus::OK,"Normal operation");
  if(!entries_.empty()){
    RefinerEntries to_keep;
    for(int i=0;i<indices.size();i++){
      if(indices[i])to_keep.push_back(entries_[i]);
    }
    entries_.swap(to_keep);
  } 
  return true;
}
void DiagStatusRefiner::CompressEntries(){
  RefinerEntries compressed;
  bool not_found=true;
  for(RefinerEntries::iterator itr(entries_.begin()), end_itr(entries_.end());
     itr != end_itr;
     ++itr) {
     for (RefinerEntries::iterator k(compressed.begin()), end_k(compressed.end());
        k!=end_k;
        ++k){
        if (itr->id_==k->id_){
          if(itr->t_+itr->expiration_ > k->t_+k->expiration_) *k=*itr;
          not_found=false;
        }
     }
     if(not_found) compressed.push_back(*itr);
  }
  entries_.swap(compressed);
}
bool DiagStatusRefiner::AddRefinerEntry(std::string id, diagnostic_updater::DiagnosticStatusWrapper s, ros::Duration expiration, ros::Time t){
  RefinerEntry entry;
  entry.id_ = id;
  entry.t_ = t;
  entry.stat_ = s;
  if(s.level>0) entry.expiration_ = expiration;
  else entry.expiration_ = ros::Duration(-1.0);
  entries_.push_back(entry);
  return true;
}

bool DiagStatusRefiner::AddRefinerEntry(std::string id, unsigned char level, std::string s, ros::Duration expiration, ros::Time t){
  if (level==0||level==1||level==2||level==3){
    RefinerEntry entry;
    entry.id_ = id;
    entry.t_ = t;
    entry.stat_.summary(level, s);
    if(level>0) entry.expiration_ = expiration;
    else entry.expiration_ = ros::Duration(-1.0);
    entries_.push_back(entry);
    return true;
  }
  return false;
}
}

