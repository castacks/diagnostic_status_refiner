/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name : diagnostic_status_refiner.h
* Purpose :
* Creation Date : 20-05-2015
* Last Modified : Wed 20 May 2015 11:34:56 AM EDT
* Created By :   Sezal Jain (sezaljain90@gmail.com)
_._._._._._._._._._._._._._._._._._._._._.*/
#ifndef DIAGNOSTIC_STATUS_REFINER_H
#define DIAGNOSTIC_STATUS_REFINER_H

#include <ros/ros.h>
#include <diagnostic_updater/diagnostic_updater.h>
//#include <std_msgs/Bool.h>
#include <diagnostic_updater/publisher.h>
namespace ca{

struct RefinerEntry{
  std::string id_;
  ros::Time t_;
  diagnostic_updater::DiagnosticStatusWrapper stat_;
  ros::Duration expiration_;
};


class DiagStatusRefiner{
  public:
    typedef std::vector<RefinerEntry> RefinerEntries;
    DiagStatusRefiner():
      status_name_("refiner"){}
    DiagStatusRefiner(std::string name):
      status_name_(name){}
    ~DiagStatusRefiner(){}
    bool AggregateEntries(diagnostic_updater::DiagnosticStatusWrapper& aggregated);
    bool AddRefinerEntry(std::string id, diagnostic_updater::DiagnosticStatusWrapper s, ros::Duration expiration= ros::Duration(-1.0), ros::Time t=ros::Time::now());
    bool AddRefinerEntry(std::string id, unsigned char level, std::string s, ros::Duration expiration= ros::Duration(-1.0), ros::Time t=ros::Time::now());
    void CompressEntries();
  private:
    RefinerEntries entries_;
    std::string status_name_;
};

class DiagHelperClass{
//This is just a stupid class as it is very hard to initialize the updater in simplistic files which dont have a class structure. 
//The problem is initialization of the updater as a global variable
  public:
    DiagHelperClass(){
      updater_ = new diagnostic_updater::Updater;
    }
    ~DiagHelperClass(){
      delete updater_;
    }
    void callback(const ros::TimerEvent& event){
      updater_->update();
    }
    diagnostic_updater::Updater *updater_;
};

}

#endif /* DIAGNOSTIC_STATUS_REFINER_H */

// Every diagnostic status can have 
// 1) one level  error/ok/warn/stale
// 2) hardware id
// 3) name --denotes what component this comes from
// 4) message -- maybe we want to display the major error here??
// 5) multiple key values -- or should the errors be here
// and should be published at low frequency

// Questions to answer
// 1) What happens to errors which accumulate faster than publish frequency
// 2) Where are the erros displayed -- multiple key values or the main message
// 3) should I separate frequency publishers from this? or use the same updater for the whole node

// How this should run
// 1) Updater called regularly from here (or msgs published directly from here -- whichever is better)
// 2) so ddo you send status from the 
// 3) If I have different components in the same updater, I can condense this to maybe one status msg? one key per componenet?
// 4) Lets do this -- the message would be the reason for error level and the time stamp ( and the key values contain all recent updates from all components)

// How about this --- it takes in an array of status and prints out a composite status? -- and this has a name;
//To check alive nodes?
